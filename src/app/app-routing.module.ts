import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AutocompleteComponent} from "./Autocomplete/autocomplete.component";

const routes: Routes = [
  { path: 'autocomplete', component: AutocompleteComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
