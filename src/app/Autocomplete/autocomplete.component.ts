//app.component.ts
import {Component, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {HttpClient} from '@angular/common/http';

import {debounceTime, tap, switchMap, finalize, distinctUntilChanged, filter} from 'rxjs/operators';
import {liveQuery} from "dexie";
import {db, SearchList} from "../services/db.service";
import {Observable} from "rxjs";

const API_KEY = "accd87d1"

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css']
})
export class AutocompleteComponent implements OnInit {

  searchMoviesCtrl = new FormControl();
  filteredMovies: SearchList[] | undefined;
  isLoading = false;
  errorMsg!: string;
  minLengthTerm = 3;
  selectedMovie: any = "";
  deferredPrompt: any;

  constructor(
    private http: HttpClient
  ) {
  }

  async addNewResult(elem: SearchList[]) {
    await db.movie.bulkAdd(elem);
  }

  homeScreenHandler() {
    if (this.deferredPrompt) {
      this.deferredPrompt.prompt();
    }

  }

  onSelected() {
    console.log(this.selectedMovie);
    this.selectedMovie = this.selectedMovie;
  }

  displayWith(value: any) {
    return value?.Title;
  }

  clearSelection() {
    this.selectedMovie = "";
    this.filteredMovies = [];
  }

  ngOnInit() {
    window.addEventListener('beforeinstallprompt', (e) => {
      // Prevent Chrome 67 and earlier from automatically showing the prompt
      e.preventDefault();
      // Stash the event so it can be triggered later.
      this.deferredPrompt = e;
    });

    this.searchMoviesCtrl.valueChanges
      .pipe(
        filter(res => {
          return res !== null && res.length >= this.minLengthTerm
        }),
        distinctUntilChanged(),
        debounceTime(1000),
        tap(() => {
          this.errorMsg = "";
          this.filteredMovies = [];
          this.isLoading = true;
        }),
        switchMap(value => {
            return new Observable((observer) => {
              db.table('movie').toArray().then((data) => {
                if (data.length > 0 && !navigator.onLine) {
                  observer.next(data);
                } else {
                  this.http.get('http://www.omdbapi.com/?apikey=' + API_KEY + '&s=' + value).subscribe((data: any) => {
                    if (data['Search']) {
                      db.table('movie').bulkAdd(data['Search']);
                    }
                    observer.next(data);
                  })
                }
              })
            });
          }
        )
      )
      .subscribe((data: any) => {
        if (data.length < 1 && data['Search'].length < 1) {
          this.errorMsg = 'No result';
          this.filteredMovies = [];
        } else {
          this.errorMsg = "";
          this.filteredMovies = data['Search'] ? data['Search'] : data;
          this.isLoading = false;
        }
        console.log(this.filteredMovies);
      });
  }
}
