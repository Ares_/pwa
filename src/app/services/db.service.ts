import {Injectable} from '@angular/core';
import Dexie, {Table} from 'dexie';

export interface SearchList {
  imdbID: string;
  Poster: string;
  Title: string;
  Type: string;
  Year: string;
}


@Injectable({
  providedIn: 'root'
})
export class DbService extends Dexie {
  movie!: Table<SearchList, number>;

  constructor() {
    super('ngdexieliveQuery');
    this.version(3).stores({
      movie: 'imdbID, Poster, Title, Type, Year',
    });
    //this.on('populate', () => this.populate());
  }

  async populate() {
    const todoListId = await db.movie.add({
      Poster: "", Type: "", Year: "", imdbID: "",
      Title: 'To Do Today'
    });
    /* await db.searchList.bulkAdd([
       {
         todoListId,
         title: 'Feed the birds',
       },
       {
         todoListId,
         title: 'Watch a movie',
       },
       {
         todoListId,
         title: 'Have some sleep',
       },
     ]);*/
  }
}

export const db = new DbService();
